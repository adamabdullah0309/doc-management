package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class ListDocumentSharedSerializable implements Serializable  {
	
	@Column(name = "list_document_share_id")
	public Integer listDocumentShareId;
	
	@Column(name = "list_document_id")
	public int listDocumentId;
	
	
	public ListDocumentSharedSerializable() {
		
	}
	
	public ListDocumentSharedSerializable(int listDocumentId,Integer listDocumentShareId)
	{
		this.listDocumentId = listDocumentId;
		this.listDocumentShareId = listDocumentShareId;
	}

	public Integer getListDocumentShareId() {
		return listDocumentShareId;
	}

	public void setListDocumentShareId(Integer listDocumentShareId) {
		this.listDocumentShareId = listDocumentShareId;
	}

	public int getListDocumentId() {
		return listDocumentId;
	}

	public void setListDocumentId(int listDocumentId) {
		this.listDocumentId = listDocumentId;
	}

	
	
	

}
