package com.example.serializable;
import javax.persistence.Embeddable;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class ListDocumentSummarySerializable implements Serializable  {
	
	@Column(name = "document_no_id")
	public Integer documentNoId;
	
	@Column(name = "id_category")
	public int idCategory;
	
	
	public ListDocumentSummarySerializable() {
		
	}
	
	public ListDocumentSummarySerializable(int idCategory,Integer documentNoId)
	{
		this.idCategory = idCategory;
		this.documentNoId = documentNoId;
	}

	public Integer getDocumentNoId() {
		return documentNoId;
	}

	public void setDocumentNoId(Integer documentNoId) {
		this.documentNoId = documentNoId;
	}

	public int getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(int idCategory) {
		this.idCategory = idCategory;
	}

	

	

	
	
	

}
