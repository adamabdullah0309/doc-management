package com.example.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocumentShared;

@Component
public interface ListDocumentSharedService {

	void save(List<ListDocumentShared> masterSharedDocument);

	List<Object[]> detailDocumentShared(UUID user_uuid, int doc_id);

	List<Object[]> listSharedWithMe(UUID user_uuid, int company_id, int group_id, int so_id, Date startDate, Date endDate,
			String status);

}
