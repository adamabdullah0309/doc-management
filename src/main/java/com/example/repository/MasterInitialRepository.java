package com.example.repository;



import com.example.entity.MasterInitial;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
	
public interface MasterInitialRepository extends JpaRepository<MasterInitial, Integer> 
{
}
