package com.example.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.entity.MasterInitial;
import com.example.service.MasterInitialService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;


@RestController
@RequestMapping("/api")
public class MasterInitialController {
	
	
	@Autowired
	private MasterInitialService MasterInitialService;
	
	
	private static final Logger logger = LoggerFactory.getLogger(MasterInitialController.class);
	
	
	
	@PostMapping({"/saveMasterInitial"})
	public String saveMasterInitial(@RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
//	    JSONObject dataHeader = (JSONObject)JSONSerializer.toJSON(header);
	    logger.info("input >>>> " + dataRequest);
	    logger.info("dataHeader >>> " + header);
	    try {
	      
	    	final String uri = "http://3.0.57.87:3003/apps/verify-token";
	        
	        RestTemplate restTemplate = new RestTemplate();
	         
	        HttpHeaders headers = new HttpHeaders();
	        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	        headers.set("x-access-code", header);
	        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
	        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
	        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
	        hasilVerivy.getBoolean("status");
	        System.out.println(result);
//	      if (arrayTask.size() > 0)
//	      {
//	    	  ArrayList<MasterInitial> isiArray = new ArrayList<>();
//	    	  for (int a = 0; a < arrayTask.size(); a++) {
////  		  Integer initialId, String initialName, String initialDecription, Integer initialStatus, 
////  		  String createdBy, LocalDateTime createdAt
//	    		  MasterInitial masterTaskList = new MasterInitial(
//  					arrayTask.getJSONObject(a).has("initialId") == false ?  null : arrayTask.getJSONObject(a).getInt("initialId"), 
//	                arrayTask.getJSONObject(a).getString("initialName"),
//	                arrayTask.getJSONObject(a).getString("initialDecription") == "" ? null : arrayTask.getJSONObject(a).getString("initialDecription"),
//	                arrayTask.getJSONObject(a).getInt("initialStatus"),
//	                arrayTask.getJSONObject(a).getString("user"),
//	                localDateTime2
//	                );
//	            isiArray.add(masterTaskList);
//	          }
//	    	  MasterInitialService.updateSave(isiArray);
//	      }
	      
	      response.put("responseCode", "00");
	      response.put("responseDesc", "berhasil");
	    } catch (Exception e) {
	      response.put("responseCode", "99");
	      response.put("responseDesc", "Save Master Initial Failed");
	      response.put("responseError", e.getMessage());
	      logger.info("ERROR #### " + e.getMessage());
	    } 
	    return response.toString();
	  }
	
	@PostMapping({"/updateTaskList"})
	  public String updateTaskList(@RequestBody String request) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try {
	    
	      LocalDateTime localDateTime = LocalDateTime.now();
	      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	      String formatDateTime = localDateTime.format(formatter);
	      LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	      
	      JSONArray arrayTask = dataRequest.getJSONArray("masterInitialArray");
	      
	      
//	      if (arrayTask.size() > 0)
//	      {
//	    	  ArrayList<MasterInitial> isiArray = new ArrayList<>();
//	    	  MasterInitial isiArray2 = new MasterInitial();
//	    	  for (int a = 0; a < arrayTask.size(); a++) {
//	    		  
//	    		  Optional<MasterInitial> dataTaskList = MTaskListService.FindByTaskListIdAndCompanyId(arrayTask.getJSONObject(a).getString("tasklistId"), dataRequest.getInt("company_id"));
//	    		  if(dataTaskList.isPresent())
//	    		  {
//	    			  ((TaskList)dataTaskList.get()).setTasklistNama(arrayTask.getJSONObject(a).getString("tasklistNama"));
//	    			  ((TaskList)dataTaskList.get()).setDescription(arrayTask.getJSONObject(a).getString("tasklistDescription"));
//	    			  ((TaskList)dataTaskList.get()).setStatus(arrayTask.getJSONObject(a).getInt("tasklistStatus"));
//	    			  ((TaskList)dataTaskList.get()).setUpdatedBy(dataRequest.getString("user"));
//	    			  ((TaskList)dataTaskList.get()).setUpdatedDate(localDateTime2);
//	    			  isiArray.add(dataTaskList.get());  
//	    		  } 
//	          }
//	    	  MTaskListService.save(isiArray);
////		  MTaskListService.save(isiArray);
//		  
//	       }
	      
	      response.put("responseCode", "00");
	      response.put("responseDesc", "Update Task List Success");
	    } catch (Exception e) {
	      response.put("responseCode", "99");
	      response.put("responseDesc", "Update Task List Failed");
	      response.put("responseError", e.getMessage());
	    } 
	    return response.toString();
	  }
}
