package com.example.controllers;
public class Child {
    private Integer childId;
    private Integer parentId;
    private Integer subcategory;
    private String value;
    private Integer subsubcategory;
    private int nourut;
    
    private Integer idDatatype;
	private String namaDatatype;
	private String namaSubcategory;
	private String namaSubsubcategory;
    public Child(Integer childId, Integer parentId, Integer subcategory, String value, Integer subsubcategory,int nourut,
    		Integer idDatatype, String namaDatatype, String namaSubcategory, String namaSubsubcategory
    		) {
        this.childId = childId;
        this.parentId = parentId;
        this.subcategory = subcategory;
        this.value = value;
        this.subsubcategory = subsubcategory;
        this.nourut = nourut;
        
        this.idDatatype = idDatatype;
        this.namaDatatype = namaDatatype;
        this.namaSubcategory = namaSubcategory;
        this.namaSubsubcategory = namaSubsubcategory;
    }
    
    
	public Integer getIdDatatype() {
		return idDatatype;
	}


	public void setIdDatatype(Integer idDatatype) {
		this.idDatatype = idDatatype;
	}


	public String getNamaDatatype() {
		return namaDatatype;
	}


	public void setNamaDatatype(String namaDatatype) {
		this.namaDatatype = namaDatatype;
	}


	public String getNamaSubcategory() {
		return namaSubcategory;
	}


	public void setNamaSubcategory(String namaSubcategory) {
		this.namaSubcategory = namaSubcategory;
	}


	public String getNamaSubsubcategory() {
		return namaSubsubcategory;
	}


	public void setNamaSubsubcategory(String namaSubsubcategory) {
		this.namaSubsubcategory = namaSubsubcategory;
	}


	public int getNourut() {
		return nourut;
	}


	public void setNourut(int nourut) {
		this.nourut = nourut;
	}


	public Integer getChildId() {
		return childId;
	}
	public void setChildId(Integer childId) {
		this.childId = childId;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public Integer getSubcategory() {
		return subcategory;
	}


	public void setSubcategory(Integer subcategory) {
		this.subcategory = subcategory;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public Integer getSubsubcategory() {
		return subsubcategory;
	}


	public void setSubsubcategory(Integer subsubcategory) {
		this.subsubcategory = subsubcategory;
	}
	
	
    
	
}