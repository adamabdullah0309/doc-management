package com.example.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.type.PostgresUUIDType;

import com.example.serializable.ListDocumentSharedSerializable;

import org.hibernate.annotations.DynamicInsert;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "list_document_shared")
@IdClass(ListDocumentSharedSerializable.class)
public class ListDocumentShared {
	@Id
	@Column(name ="list_document_share_id")
	private Integer listDocumentShareId;
	
	@Type(type="pg-uuid")
	@Column(name="list_document_share_user")
	private UUID listDocumentShareUser;
	
	@Column(name = "list_document_share_start_date")
	private Date listDocumentShareStartDate;
	
	@Column(name = "list_document_share_end_date")
	private Date listDocumentShareEndDate;
	
	@Column(name = "list_document_share_role")
	private int listDocumentShareRole;
	
	@Column(name = "list_document_share_user_group")
	private Integer listDocumentShareUserGroup;
	
	@Column(name = "list_document_share_so")
	private Integer listDocumentShareSo;
	
	@Id
	@Column(name = "list_document_id")
	private int listDocumentId;
	
	@Type(type="pg-uuid")
	@Column(name = "user_id")
	private UUID userId;
	
	@Column(name = "company_id")
	private int companyId;
	
	public ListDocumentShared() {}
	
	public ListDocumentShared(Integer listDocumentShareId, UUID listDocumentShareUser, Date listDocumentShareStartDate,Date listDocumentShareEndDate,
			int listDocumentShareRole, Integer listDocumentShareUserGroup, Integer listDocumentShareSo, int listDocumentId,
			UUID userId, int companyId) 
	{
		this.listDocumentShareId = listDocumentShareId;
		this.listDocumentShareUser = listDocumentShareUser;
		this.listDocumentShareStartDate = listDocumentShareStartDate;
		this.listDocumentShareEndDate = listDocumentShareEndDate;
		this.listDocumentShareRole = listDocumentShareRole;
		this.listDocumentShareUserGroup = listDocumentShareUserGroup;
		this.listDocumentShareSo = listDocumentShareSo;
		this.listDocumentId = listDocumentId;
		this.userId = userId;
		this.companyId = companyId;
	}

	public Integer getListDocumentShareId() {
		return listDocumentShareId;
	}

	public void setListDocumentShareId(Integer listDocumentShareId) {
		this.listDocumentShareId = listDocumentShareId;
	}

	public UUID getListDocumentShareUser() {
		return listDocumentShareUser;
	}

	public void setListDocumentShareUser(UUID listDocumentShareUser) {
		this.listDocumentShareUser = listDocumentShareUser;
	}

	public Date getListDocumentShareStartDate() {
		return listDocumentShareStartDate;
	}

	public void setListDocumentShareStartDate(Date listDocumentShareStartDate) {
		this.listDocumentShareStartDate = listDocumentShareStartDate;
	}

	public Date getListDocumentShareEndDate() {
		return listDocumentShareEndDate;
	}

	public void setListDocumentShareEndDate(Date listDocumentShareEndDate) {
		this.listDocumentShareEndDate = listDocumentShareEndDate;
	}

	public int getListDocumentShareRole() {
		return listDocumentShareRole;
	}

	public void setListDocumentShareRole(int listDocumentShareRole) {
		this.listDocumentShareRole = listDocumentShareRole;
	}

	public int getListDocumentShareUserGroup() {
		return listDocumentShareUserGroup;
	}

	public void setListDocumentShareUserGroup(int listDocumentShareUserGroup) {
		this.listDocumentShareUserGroup = listDocumentShareUserGroup;
	}

	public int getListDocumentShareSo() {
		return listDocumentShareSo;
	}

	public void setListDocumentShareSo(int listDocumentShareSo) {
		this.listDocumentShareSo = listDocumentShareSo;
	}

	public int getListDocumentId() {
		return listDocumentId;
	}

	public void setListDocumentId(int listDocumentId) {
		this.listDocumentId = listDocumentId;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	
	
	
}
