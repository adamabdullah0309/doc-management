package com.example.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.ListDocument;
import com.example.entity.ListDocumentShared;
import com.example.entity.ListDocumentSummary;
import com.example.entity.ListDocumentSummarySubcategory;
import com.example.service.GabunganService;
import com.example.service.ListDocumentService;
import com.example.service.ListDocumentSharedService;
import com.example.service.ListDocumentSummaryService;
import com.example.service.ListDocumentSummarySubcategoryService;
@Service
public class GabunganServiceImpl implements GabunganService {
	
	@Autowired
	private ListDocumentSharedService ListDocumentSharedService;
	
	@Autowired
	private ListDocumentService ListDocumentService;
	
	@Autowired 
	private ListDocumentSummaryService ListDocumentSummaryService;
	
	@Autowired
	private ListDocumentSummarySubcategoryService ListDocumentSummarySubcategoryService;

	@Override
	@Transactional
	public void SaveShared(ListDocument data1, List<ListDocumentShared> data2) {
		// TODO Auto-generated method stub
		ListDocumentService.saveDoc(data1);
		ListDocumentSharedService.save(data2);
	}

	@Override
	@Transactional
	public ListDocument saveSummary(ListDocument lastId, List<ListDocumentSummary> dataCategory,
			List<ListDocumentSummarySubcategory> newData) {
		ListDocument LastId = ListDocumentService.saveDoc(lastId);
		ListDocumentSummaryService.save(dataCategory,LastId);
		ListDocumentSummarySubcategoryService.save(newData,LastId);
		return LastId;
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public void updateSummary(ListDocument listDocument, ArrayList<ListDocumentSummary> dataCategory,
			ArrayList<ListDocumentSummarySubcategory> newData) {
		// TODO Auto-generated method stub
		ListDocument LastId = ListDocumentService.saveDoc(listDocument);
		ListDocumentSummaryService.delete(LastId.getListDocumentId(), LastId.getIdCompany());
		ListDocumentSummarySubcategoryService.delete(LastId.getListDocumentId(), LastId.getIdCompany());
		ListDocumentSummaryService.save(dataCategory,LastId);
		ListDocumentSummarySubcategoryService.save(newData,LastId);
	}

	@Override
	@Transactional
	public void updateSummary2(ListDocument listDocument, ArrayList<ListDocumentSummary> dataCategory,
			ArrayList<ListDocumentSummarySubcategory> newData) {
		// TODO Auto-generated method stub
		ListDocument LastId = ListDocumentService.saveDoc(listDocument);
		ListDocumentSummaryService.save(dataCategory,LastId);
		ListDocumentSummarySubcategoryService.save(newData,LastId);
	}

}
