package com.example.entity;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;


@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "master_initial")
//@IdClass(MTaskListSerializable.class)
//@NamedQueries
//	(
//		{
//			@NamedQuery(name="TaskList.findByStatusAndCompanyId", query = "FROM TaskList e WHERE e.status =?1 and e.companyId = ?2 order by createdDate desc"),
//	    	@NamedQuery(name="TaskList.findByCompanyId", query = "FROM TaskList e WHERE e.companyId = ?1 order by createdDate desc"),
////	    	@NamedQuery(name = "TaskList.findByBarangIdAndCompanyId", query = "FROM Barang e where e.barangId = ?1 and e.companyId =?2")
//		}
//	)
public class MasterInitial {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "master_initial_sequence")
	@SequenceGenerator(sequenceName = "master_initial_sequence",name = "master_initial_sequence",initialValue=0,  allocationSize = 1)
  @Column(name = "initial_id")
  private Integer initialId;
  
  @Column(name = "initial_name")
  private String initialName;
  
  @Column(name = "initial_decription")
  private String initialDecription;
  
  @Column(name = "initial_status")
  private Integer initialStatus;
  
  @Column(name = "created_by")
  private String createdBy;
  
//  @UpdateTimestamp
  @Column(name = "created_at")
  private LocalDateTime createdAt;
  
  @Column(name = "updated_by")
  private String updatedBy;
  
  @Column(name = "updated_at")
  private LocalDateTime updatedAt;
  
  public MasterInitial(Integer initialId, String initialName, String initialDecription, Integer initialStatus, 
		  String createdBy, LocalDateTime createdAt) {
	    this.initialId = initialId;
	    this.initialName = initialName;
	    this.initialDecription = initialDecription;
	    this.initialStatus = initialStatus;
	    this.createdBy = createdBy;
	    this.createdAt = createdAt;
  }
  
  public MasterInitial() {}

public Integer getInitialId() {
	return initialId;
}

public void setInitialId(Integer initialId) {
	this.initialId = initialId;
}

public String getInitialName() {
	return initialName;
}

public void setInitialName(String initialName) {
	this.initialName = initialName;
}

public String getInitialDecription() {
	return initialDecription;
}

public void setInitialDecription(String initialDecription) {
	this.initialDecription = initialDecription;
}

public Integer getInitialStatus() {
	return initialStatus;
}

public void setInitialStatus(Integer initialStatus) {
	this.initialStatus = initialStatus;
}

public String getCreatedBy() {
	return createdBy;
}

public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
}

public LocalDateTime getCreatedAt() {
	return createdAt;
}

public void setCreatedAt(LocalDateTime createdAt) {
	this.createdAt = createdAt;
}

public String getUpdatedBy() {
	return updatedBy;
}

public void setUpdatedBy(String updatedBy) {
	this.updatedBy = updatedBy;
}

public LocalDateTime getUpdatedAt() {
	return updatedAt;
}

public void setUpdatedAt(LocalDateTime updatedAt) {
	this.updatedAt = updatedAt;
}
  
  
  
}
