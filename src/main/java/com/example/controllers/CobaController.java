package com.example.controllers;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import com.example.controllers.Helpers;
import com.example.entity.ListDocumentSummary;
import com.example.entity.*;
import com.example.service.*;
import com.example.service.ListDocumentSummaryService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/api")
public class CobaController {
	private static final Logger logger = LoggerFactory.getLogger(CobaController.class);
	Object data;
	
	@Autowired
	private ListDocumentSummarySubcategoryService ListDocumentSummarySubcategoryService;
	
	@Autowired
	private ListDocumentSummaryService ListDocumentSummaryService;
	
	@PostMapping("/hello")
	public String hello(@RequestBody String request)
	{
		return "hello";
	}
	
	@PostMapping("/read")
	public String read(@RequestBody String request)
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    
	    if(dataRequest.getJSONArray("detailData").size() > 0)
	    {
	    	ListDocumentSummaryService.delete(dataRequest.getInt("listDocumentId"), dataRequest.getInt("company_id"));
	    	ListDocumentSummarySubcategoryService.delete(dataRequest.getInt("listDocumentId"), dataRequest.getInt("company_id"));
	    	for(int a = 0 ; a < dataRequest.getJSONArray("detailData").size(); a++)
	    	{
	    		JSONArray dataChild = (JSONArray) JSONSerializer.toJSON(dataRequest.getJSONArray("detailData").getJSONObject(a).getJSONArray("detail"));
	    		
	    		
	    		ListDocumentSummary data = new ListDocumentSummary();
	    		data.setDocumentNoId(dataRequest.getInt("listDocumentId"));
	    		data.setIdCompany(dataRequest.getInt("company_id"));
	    		data.setIdCategory(dataRequest.getJSONArray("detailData").getJSONObject(a).getInt("category"));
//	    		ListDocumentSummaryService.save(data);
	    		
	    		hasChild(dataChild,dataRequest.getInt("listDocumentId"),dataRequest.getJSONArray("detailData").getJSONObject(a).getInt("category"),dataRequest.getInt("company_id"));
	    	}
	    }
		return "halo";
	}
	
	 public void  hasChild(JSONArray childData, int doc, int category, int company){
	    	List<JSONObject> cont = new ArrayList<>();
			JSONObject response = new JSONObject();
			 ListDocumentSummarySubcategory data = new ListDocumentSummarySubcategory();

	              for (int i = 0; i < childData.size(); i++) {
	                   JSONObject resultData = (JSONObject) childData.get(i);
	                   
	                   
	                   data.setDocumentNoId(doc);
	                   data.setIdCompany(company);
	                   data.setIdCategory(category);
	                   data.setIdSubcategory(resultData.getInt("subcategory"));
	                   data.setValue(resultData.getString("value"));
	                   data.setIdSubsubcategory(resultData.getInt("subsubcategory"));
	                   data.setNourut(resultData.getInt("nourut"));
	                   data.setParentId(resultData.getInt("parentId"));
	                   data.setId(resultData.getInt("id"));
//	                   ListDocumentSummarySubcategoryService.save(data);
	                   logger.info("data = " + data.getId());
	                   
//	                   // disiini olah data nya, misal insert ke tabel
	                   JSONArray dataAnotherChild = (JSONArray) JSONSerializer.toJSON( resultData .get("childrenItems"));
	                   if(dataAnotherChild.size() > 0){
	                         hasChild(dataAnotherChild, doc, category, company);
	                         
	                   }
	              }
	    	
	    }
}
