package com.example.serviceimpl;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.*;
import com.example.repository.ListDocumentSummaryRepository;
import com.example.service.ListDocumentSummaryService;
import com.example.serializable.ListDocumentSummarySerializable;

@Service
public class ListDocumentSummaryServiceImpl implements ListDocumentSummaryService {
	private static final Logger logger = LoggerFactory.getLogger(ListDocumentSummaryServiceImpl.class);
	
	@Autowired
	private ListDocumentSummaryRepository ListDocumentSummaryRepository;
	
	@Autowired
	EntityManager em;

	@Override
	public void save(List<ListDocumentSummary> data, ListDocument Id) {
		// TODO Auto-generated method stub
		
		for (ListDocumentSummary tasklistLooping : data) {
//			em.persist(tasklistLooping);
			tasklistLooping.setDocumentNoId(Id.getListDocumentId());
			ListDocumentSummaryRepository.save(tasklistLooping);
		}
//		ListDocumentSummaryRepository.save(data);
	}

	@Override 
	@Transactional
	public void delete(int doc, int companyId) {
		// TODO Auto-generated method stub
		em.createNativeQuery("delete from list_document_summary where document_no_id = :id and id_company = :companyId")
		.setParameter("id", doc).setParameter("companyId", companyId).executeUpdate();
	}

	@Override
	public List<Object[]> listSubcategory(String paramAktif, int company) {
		// TODO Auto-generated method stub
		String cari = "";
		if (paramAktif.equals("y"))
		{
			cari  = " and a.status = \'t\' ";
		}
		else if(paramAktif.equals("n"))
		{
			cari  = " and a.status = \'f\' ";
		}
		else
		{
			cari = "";
		}
		return em.createNativeQuery("SELECT A\r\n" + 
				"	.ID AS idsub,\r\n" + 
				"	A.caption AS namasub,\r\n" + 
				"	A.status,\r\n" + 
				"	A.id_company,\r\n" + 
				"	b.datatype_id,\r\n" + 
				"	b.datatype_name \r\n" + 
				"FROM\r\n" + 
				"	\"m_subcategory\"\r\n" + 
				"	AS A LEFT JOIN m_datatype AS b ON A.datatype_id = b.datatype_id\r\n" + 
				"	where\r\n" + 
				"	a.id_company = :company" + cari)
				.setParameter("company", company).getResultList();
	}

	@Override
	public List<Object[]> listSubSubcategory(int company, int subcategoryId) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT \r\n" + 
				"	a.id as subsubcategoryid,\r\n" + 
				"	a.caption as subsubcategorynama\r\n" + 
				 
				"FROM\r\n" + 
				"	\"m_subsubcategory\"\r\n" + 
				"	AS A " + 
				"	where\r\n" + 
				"	a.id_company = :company and a.id_subcategory = :subcategory ")
				.setParameter("company", company)
				.setParameter("subcategory", subcategoryId)
				.getResultList();
	}

	@Override
	public List<Object[]> listCategory(String paramAktif, int company) {
		// TODO Auto-generated method stub
		String cari = "";
		if (paramAktif.equals("y"))
		{
			cari  = " and a.status = \'t\' ";
		}
		else if(paramAktif.equals("n"))
		{
			cari  = " and a.status = \'f\' ";
		}
		else
		{
			cari = "";
		}
		return em.createNativeQuery("SELECT A\r\n" + 
				"	.ID,\r\n" + 
				"	A.caption,\r\n" + 
				"	A.id_company,\r\n" + 
				"	A.status \r\n" + 
				"FROM\r\n" + 
				"	\"m_category\" AS A \r\n" + 
				"WHERE\r\n" + 
				" A.id_company = :company " + cari)
				.setParameter("company", company).getResultList();
	}

	@Override
	public List<Object[]> listDocumentType(String paramAktif, int company) {
		// TODO Auto-generated method stub
		String cari = "";
		if (paramAktif.equals("y"))
		{
			cari  = " and a.status = \'t\' ";
		}
		else if(paramAktif.equals("n"))
		{
			cari  = " and a.status = \'f\' ";
		}
		else
		{
			cari = "";
		}
		return em.createNativeQuery("SELECT A\r\n" + 
				"	.ID,\r\n" + 
				"	A.caption,\r\n" + 
				"	A.id_company,\r\n" + 
				"	A.status \r\n" + 
				"FROM\r\n" + 
				"	\"m_document_type\" AS A \r\n" + 
				"WHERE\r\n" + 
				" A.id_company = :company " + cari)
				.setParameter("company", company).getResultList();
	}

}
