package com.example.service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocument;
import com.example.entity.ListDocumentShared;
import com.example.entity.ListDocumentSummary;
import com.example.entity.ListDocumentSummarySubcategory;
import com.example.entity.MasterInitial;

@Component
public interface GabunganService {
	public void SaveShared(ListDocument data1, List<ListDocumentShared> data2);

	public ListDocument saveSummary(ListDocument lastId, List<ListDocumentSummary> dataCategory,
			List<ListDocumentSummarySubcategory> newData);

	public void updateSummary(ListDocument listDocument, ArrayList<ListDocumentSummary> dataCategory,
			ArrayList<ListDocumentSummarySubcategory> newData);
	
	public void updateSummary2(ListDocument listDocument, ArrayList<ListDocumentSummary> dataCategory,
			ArrayList<ListDocumentSummarySubcategory> newData);
}
