package com.example.config;

import java.net.URISyntaxException;

import org.springframework.http.ResponseEntity;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

public class Constants {
	public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 5*60*60;
    public static final String SIGNING_KEY = "accservices123";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    
    public static JSONObject checkProAccess(String accessCode) throws URISyntaxException {
    	RestClient client = new RestClient();
		client.setApiUrl("apps/verify-token");
		client.setHeaders("x-access-code", accessCode);
		
		ResponseEntity<String> resp = client.get();
		JSONObject dataRespAccess = (JSONObject) JSONSerializer.toJSON(resp.getBody());
    	return dataRespAccess;
    }
}
