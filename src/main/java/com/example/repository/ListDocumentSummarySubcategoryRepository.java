package com.example.repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.example.entity.ListDocumentSummarySubcategory;
import com.example.serializable.ListDocumentSummarySubcategorySerializable;


public interface ListDocumentSummarySubcategoryRepository extends JpaRepository<ListDocumentSummarySubcategory, ListDocumentSummarySubcategorySerializable> {

}
